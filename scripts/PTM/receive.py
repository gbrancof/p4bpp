import datetime
from bpp import *
from scapy.all import sniff
import time

i = 0

start = time.time()

def measurement(t):
    micro = t % 1000
    milli = (t - micro) // 1000 % 1000
    sec = (t - micro - 1000 * milli) // 1000000 % 1000

    return str(sec) + "." + str(milli) + "s"


def parse(packet):
    global i
    i += 1
    #print(str(time.time() - start) + ","+measurement(packet[BPPMetadata].data1) + ","+measurement(packet[BPPMetadata].data2))
    print(str(time.time() - start) + ","+measurement(packet[BPPMetadata].data1)+","+measurement(packet[BPPMetadata].data2))


def main():
    sniff(filter="ether proto 0x0800 and ip proto 0xfd", count=0, prn=parse)


if __name__ == '__main__':
    main()
