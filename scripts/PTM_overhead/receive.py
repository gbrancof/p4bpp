import datetime
from bpp import *
from scapy.all import sniff
import time
import sys

i = 0
time_last = -1
resolution = 1000
nlogs = 0
prefix_log = sys.argv[1]

def measurement(t):
    micro = t % 1000
    milli = (t - micro) // 1000 % 1000
    sec = (t - micro - 1000 * milli) // 1000000 % 1000

    return str(sec) + "." + str(milli) + "s"


def parse(packet):
    global i
    global time_last
    current = time.time()
    global nlogs
    if time_last < 0:
        time_last = current
    elif i%resolution == 0:
        #print("Througput(pkt/sec):"+str(1.0*resolution/(current-time_last)))
        print(prefix_log+","+str(1.0*resolution/(current-time_last)))
        nlogs+=1
        time_last = current

    if nlogs>10:
        sys.exit()
    i += 1

def main():
    sniff(filter="ether proto 0x0800 and ip proto 0xfd", count=0, prn=parse)


if __name__ == '__main__':
    main()
