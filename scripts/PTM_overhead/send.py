import sys
import time
from bpp import *
from scapy.all import sendp, conf
import socket

def main():
    if len(sys.argv) < 3 :
        print("Usage: python send.py [IP] [packet amount] [Delay between packets in sec].")
    else:
        i = 1
        wait = 1

        if len(sys.argv) > 2:
            try:
                i = int(sys.argv[2])
            except ValueError:
                print("Could not parse argument into int.")

        if len(sys.argv) > 3:
            try:
                wait = int(sys.argv[3])
            except ValueError:
                print("Could not parse argument into int.")

        packet = IP(dst=sys.argv[1], proto=IP_BPP) / BPPHeader(next=BPP_COMMAND) / \
                 BPPCommand(
                     length=0x0000,
                     serialized=0x00,

                     actionLength=0x00000001,

                     a1Length=0x02,
                     a1Type=BPP_ACTION_SUM,

                     a1p1Category=BPP_PARAMETER_CATEGORY_BUILTIN,
                     a1p1Value=0x01,

                     a1p2Category=BPP_PARAMETER_CATEGORY_METADATA,
                     a1p2Value=0x01,

                     next=BPP_COMMAND) / \
                 BPPCommand(
                     length=0x0000,
                     serialized=0x00,

                     actionLength=0x00000001,

                     a1Length=0x02,
                     a1Type=BPP_ACTION_SUM,

                     a1p1Category=BPP_PARAMETER_CATEGORY_BUILTIN,
                     a1p1Value=0x02,

                     a1p2Category=BPP_PARAMETER_CATEGORY_METADATA,
                     a1p2Value=0x02,

                     next=BPP_METADATA) / \
                 BPPMetadata(next=BPP_ICMP) / ICMP()


        #packet.show()

        #s = conf.L3socket()
        #packet =  IP(dst=sys.argv[1]) / ICMP()
        packet.show()
        b_packet = bytes(packet)

        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_RAW)
        except socket.error , msg:
            print 'Socket could not be created. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
            sys.exit()


        for j in range(i):
            print("Sending packet " + str(j) + "...")
            time.sleep(float(sys.argv[3]))
            s.sendto(b_packet, (sys.argv[1], 0 ))
            #s.send(packet)
            #sendp(packet)
            #time.sleep(wait)


        #print(str(i) + " packets sent.")


if __name__ == '__main__':
    main()
