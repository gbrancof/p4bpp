import sys
import time
from bpp import *
from scapy.all import sr1, sendp, sniff


def average(arr):
    avg = 0
    for i in range(len(arr)):
        avg += arr[i]

    if len(arr) > 0:
        avg /= len(arr)

    return avg


def get_time(t):
    ms = round(t*1000) % 1000
    mics = round(t*1000*1000) % 1000

    return str(int(ms)) + "ms" + str(int(mics)) + "mics"


def main():
    if len(sys.argv) < 2:
        print("Usage: python send.py <IP> [packet amount].")
    else:
        i = 1
        wait = 1

        if len(sys.argv) > 2:
            try:
                i = int(sys.argv[2])
            except ValueError:
                print("Could not parse argument into int.")

        packet = IP(dst=sys.argv[1], ttl=20) / ICMP()

        delays = []
        print("Running test with a simple ping packet...")
        for j in range(i):
            start = time.time()
            reply = sr1(packet, verbose=0)
            end = time.time()

            delays.append(end - start)

        avg = average(delays)
        print("Average packet transmission time : " + get_time(avg / 2))

        packet = Ether(type=0x0800) / IP(dst=sys.argv[1], proto=IP_BPP) / BPPHeader(next=BPP_COMMAND) / \
                 BPPCommand(
                     length=0x0000,
                     serialized=0x00,

                     actionLength=0x00000001,

                     a1Length=0x02,
                     a1Type=BPP_ACTION_SUM,

                     a1p1Category=BPP_PARAMETER_CATEGORY_BUILTIN,
                     a1p1Value=0x01,

                     a1p2Category=BPP_PARAMETER_CATEGORY_METADATA,
                     a1p2Value=0x01,

                     next=BPP_COMMAND) / \
                 BPPCommand(
                     length=0x0000,
                     serialized=0x00,

                     actionLength=0x00000001,

                     a1Length=0x02,
                     a1Type=BPP_ACTION_SUM,

                     a1p1Category=BPP_PARAMETER_CATEGORY_BUILTIN,
                     a1p1Value=0x02,

                     a1p2Category=BPP_PARAMETER_CATEGORY_METADATA,
                     a1p2Value=0x02,

                     next=BPP_COMMAND) / \
                 BPPCommand(
                     length=0x0000,
                     serialized=0x00,

                     conditionLength=0x0001,
                     conditionType=0x0000,

                     c1Length=0x02,
                     c1Negation=0x00,
                     c1Flags=0x00,
                     c1Type=BPP_CONDITION_EQUAL,

                     c1p1Category=BPP_PARAMETER_CATEGORY_STATELET,
                     c1p1Length=0x0000,
                     c1p1Value=1,

                     c1p2Category=BPP_PARAMETER_CATEGORY_METADATA,
                     c1p2Length=0x0000,
                     c1p2Value=0x00,

                     actionLength=0x00000001,

                     a1Length=0x02,
                     a1Type=BPP_ACTION_PUT,

                     a1p1Category=BPP_PARAMETER_CATEGORY_BUILTIN,
                     a1p1Value=0x03,

                     a1p2Category=BPP_PARAMETER_CATEGORY_METADATA,
                     a1p2Value=0x01,

                     next=BPP_COMMAND) / \
                 BPPCommand(
                     length=0x0000,
                     serialized=0x00,

                     conditionLength=0x0001,
                     conditionType=0x0000,

                     c1Length=0x02,
                     c1Negation=0x00,
                     c1Flags=0x00,
                     c1Type=BPP_CONDITION_EQUAL,

                     c1p1Category=BPP_PARAMETER_CATEGORY_STATELET,
                     c1p1Length=0x0000,
                     c1p1Value=2,

                     c1p2Category=BPP_PARAMETER_CATEGORY_METADATA,
                     c1p2Length=0x0000,
                     c1p2Value=0x00,

                     actionLength=0x00000001,

                     a1Length=0x02,
                     a1Type=BPP_ACTION_PUT,

                     a1p1Category=BPP_PARAMETER_CATEGORY_BUILTIN,
                     a1p1Value=0x03,

                     a1p2Category=BPP_PARAMETER_CATEGORY_METADATA,
                     a1p2Value=0x02,

                     next=BPP_COMMAND) / \
                 BPPCommand(
                     length=0x0000,
                     serialized=0x00,

                     conditionLength=0x0001,
                     conditionType=0x0000,

                     c1Length=0x02,
                     c1Negation=0x00,
                     c1Flags=0x00,
                     c1Type=BPP_CONDITION_EQUAL,

                     c1p1Category=BPP_PARAMETER_CATEGORY_STATELET,
                     c1p1Length=0x0000,
                     c1p1Value=3,

                     c1p2Category=BPP_PARAMETER_CATEGORY_METADATA,
                     c1p2Length=0x0000,
                     c1p2Value=0x00,

                     actionLength=0x00000001,

                     a1Length=0x02,
                     a1Type=BPP_ACTION_PUT,

                     a1p1Category=BPP_PARAMETER_CATEGORY_BUILTIN,
                     a1p1Value=0x03,

                     a1p2Category=BPP_PARAMETER_CATEGORY_METADATA,
                     a1p2Value=0x03,

                     next=BPP_METADATA) / \
                 BPPMetadata(next=BPP_ICMP) / ICMP()

        delays = []
        print("Running test with a BPP ping packet...")
        for j in range(i):
            start = time.time()
            sendp(packet, verbose=0)
            packet[IP].id += 1
            reply = sniff(filter="ether proto 0x0800 and ip proto 0xfd", count=1)
            end = time.time()

            delays.append(end - start)

        avg = average(delays)
        print("Average BPP packet transmission time : " + get_time(avg / 2))


if __name__ == '__main__':
    main()
