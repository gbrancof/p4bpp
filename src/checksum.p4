#ifndef _CHECKSUM_P4_
#define _CHECKSUM_P4_

#include "headers/headers.p4"
#include "metadata.p4"

control BPPVerifyChecksum(inout headers hdr, inout metadata meta) {
    apply {

    }
}

control BPPComputeChecksum(inout headers  hdr, inout metadata meta) {
     apply {
	update_checksum(
	    hdr.ip.ipv4.isValid(),
            { hdr.ip.ipv4.version,
	          hdr.ip.ipv4.ihl,
              hdr.ip.ipv4.tos,
              hdr.ip.ipv4.length,
              hdr.ip.ipv4.id,
              hdr.ip.ipv4.flags,
              hdr.ip.ipv4.fragOffset,
              hdr.ip.ipv4.ttl,
              hdr.ip.ipv4.protocol,
              hdr.ip.ipv4.srcAddr,
              hdr.ip.ipv4.dstAddr },
            hdr.ip.ipv4.checksum,
            HashAlgorithm.csum16);
    }
}

#endif